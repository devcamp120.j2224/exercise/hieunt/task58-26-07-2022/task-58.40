package com.task58s40.productapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task58s40.productapi.model.CProduct;
import com.task58s40.productapi.repository.IProductRepository;

@RestController
@CrossOrigin
public class CProductController {
    @Autowired
    IProductRepository productRepository;
    @GetMapping("/products")
    public ResponseEntity<List<CProduct>> getAllProducts() {
        try {
            List<CProduct> productList = new ArrayList<CProduct>();
            productRepository.findAll().forEach(productList::add);
            return new ResponseEntity<>(productList, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
