package com.task58s40.productapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task58s40.productapi.model.CProduct;

public interface IProductRepository extends JpaRepository <CProduct, Long> {
    
}
