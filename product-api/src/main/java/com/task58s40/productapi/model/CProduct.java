package com.task58s40.productapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class CProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "ten_san_pham")
    private String productName;
    @Column(name = "ma_san_pham")
    private String productCode;
    @Column(name = "don_gia")
    private long price;
    @Column(name = "ngay_tao")
    private long ngayTao;
    @Column(name = "ngay_cap_nhat")
    private long ngayCapNhat;    
    public CProduct() {
    }
    public CProduct(long id, String productName,
    String productCode, long price, long ngayTao, long ngayCapNhat) {
        this.id = id;
        this.productName = productName;
        this.productCode = productCode;
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProductCode() {
        return productCode;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public long getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }
    public long getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }    
}
